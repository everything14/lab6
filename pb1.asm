.386
.model flat, stdcall
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;includem msvcrt.lib, si declaram ce functii vrem sa importam
includelib msvcrt.lib
extern printf: proc
extern scanf: proc
extern exit: proc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;declaram simbolul start ca public - de acolo incepe executia
public start
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;sectiunile programului, date, respectiv cod
.data
format_introduceti_numar db "Introduceti numarul %d: ", 0
format_scanf db "%d", 0
format_suma db "Suma celor doua numere este: %d", 13, 10, 0

numar1 dd ?
numar2 dd ?

.code
start:
	push 1
	push offset format_introduceti_numar
	call printf
	add esp, 8
	
	; scanf(format, numar)
	push offset numar1
	push offset format_scanf
	call scanf
	add esp, 8
	
	
	
	push 2
	push offset format_introduceti_numar
	call printf
	add esp, 8
	
	push offset numar2
	push offset format_scanf
	call scanf
	add esp, 8
	
	
	
	mov eax, numar1
	add eax, numar2
	
	push eax
	push offset format_suma
	call printf
	add esp, 8
	
	push 0
	call exit
end start
